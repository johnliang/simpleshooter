﻿using UnityEngine;
using System.Collections;

//This script manages the player object
public class Player : Spaceship
{
    //Manager gameManger;


    void start()
    {
        GameObject theManager = GameObject.Find("Life");

        Manager manager = theManager.GetComponent<Manager>();
        
    }

	void Update ()
	{
		//Get our raw inputs
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		//Normalize the inputs
		Vector2 direction = new Vector2 (x, y).normalized;
		//Move the player
		Move (direction);
        shotDelay = 0.1f;
        
    }

    void Move(Vector2 direction)
	{
		//Find the screen limits to the player's movement
		Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
       double minX;
        minX = 0.6;
		//Get the player's current position
		Vector2 pos = transform.position;
		//Calculate the proposed position
		pos += direction  * speed * Time.deltaTime;
		//Update the player's position
		
        
         if(transform.position.x < min.x)
        {
            pos.x = min.x;
        }
         if (transform.position.x > max.x)
        {
            pos.x = max.x;
        }

         if (transform.position.y < min.y)
        {
            pos.y = min.y;
        }

        if (transform.position.y > max.y)
        {
            pos.y = max.y;
        }
        transform.position = pos;
    }

	void OnTriggerEnter2D (Collider2D c)
	{
		//Get the layer of the collided object
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		//If the player hit an enemy bullet or ship...
		if(layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			//...and the object was a bullet...
			if(layerName == "Bullet (Enemy)" )
				//...return the bullet to the pool...
			    ObjectPool.current.PoolObject(c.gameObject) ;
			//...otherwise...
			else
				//...deactivate the enemy ship
				c.gameObject.SetActive(false);
            //gameManger.lifes--;
            //Tell the manager that we crashed

            if (Manager.current.lifes <= 0)
            {
                Explode();
                Manager.current.GameOver();
                //Trigger an explosion
                Explode();
                //Deactivate the player
                gameObject.SetActive(false);
            }

            Manager.current.lifes--;
            gameObject.SetActive(false);
            gameObject.SetActive(true);
            
        }
	}
}